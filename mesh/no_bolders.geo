
cl1 = 6;
cl2 = .03;
cl3 = 10;

scale = 1.0e-2; // convert to cm
extr = 30 * scale;                                          
extr_bottom = 6 * scale;                                          // length of domain prependicular to surface (Z direction)
lds = 4.5;                                                        // length of downstream
w = 40 * scale;


Point(1) = {0,0, 0, cl1};
//+
Point(2) = { 0, 2*w, 0, cl3};
//+
Point(3) = { lds,  0, 0, cl3};
//+
Point(4) = {lds, 2* w, 0, cl1};
//+

Line(1) = {2, 4};
//+
Line(2) = {4, 3};
//+
Line(3) = {3, 1};
//+
Line(4) = {1, 2};
//+
Curve Loop(1) = {1, 2, 3, 4};
//+
Plane Surface(1) = {1};
//+
grad_ver = 0.5;   // grading in diagonal lines
grad_hor = 1.008;   // grading downstream horizonal lines
cell_num_ver= 100;   // num of cell vertical lines up/down of cylinder 
cell_num_hor = 250; // num of cell over diagonal lines insde outer cylinder
Transfinite Line {1,-3}= cell_num_hor Using Progression grad_hor;
Transfinite Line {2,4} = cell_num_ver Using Bump grad_ver;  // downstream vertical lines
//+
layer_botom = 12;
layer_up = 30;
layer_middle = 8;
Transfinite Surface{1};
Recombine Surface {1};

Extrude {0, 0, extr_bottom} { Surface{1}; Layers{layer_botom}; Recombine;}


Extrude {0, 0, extr - extr_bottom} { Surface{26};Layers{{5,8,20},{0.1,0.3,1.0}}; Recombine;}


Transfinite Surface{1,26};
Recombine Surface {1,26};
Physical Surface("symmetric") = {26};
Physical Surface("inlet") = {47,25};
Physical Surface("outlet") = {17,39};
Physical Surface("wall") ={21,43,13,35};
Physical Surface("bottom") ={1};
Physical Surface("atmosphere") ={48};
Physical Volume("internalField") ={1,2};

