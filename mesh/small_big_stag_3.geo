
cl1 = 6;
cl2 = .03;
cl3 = 10;

scale = 1.0e-2; // convert to cm
bolder_width = 6 * scale;
small_bolder_height = 6 * scale;
dist_2bolder_x = 7 * bolder_width;            // distance between center of two bolders in x direction
dist_2bolder_y = 4 * bolder_width;          // distance between center of two bolders in y direction
out_rad_x = dist_2bolder_x / 2;
out_rad_y = dist_2bolder_y / 2;
extr_small_bolder = 6 * scale;                // length of domain prependicular to surface (Z direction)
extr = 30 * scale;                                          // length of domain prependicular to surface (Z direction)
lus = bolder_width * 40;             // length of upstream
lds = bolder_width * 30;             // length of downstream
w = 40 * scale;                                              // Width of domain


/* (0,0,0) is the center of big bolder */
// Exterior (bounding box) of mesh
Point(1) = {-lus, -w, 0, cl1};
//+
Point(2) = { lds + dist_2bolder_x, -w, 0, cl3};
//+
Point(3) = { lds + dist_2bolder_x,  w, 0, cl3};
//+
Point(4) = {-lus,  w, 0, cl1};
//+

// Circle & surrounding structured-quad region
//+
Point(5) = {bolder_width / 2,  bolder_width / 2, 0, cl2};
//+
Point(6) = {bolder_width/ 2, -bolder_width/ 2, 0, cl2};
//+
Point(7) = {-bolder_width / 2,  bolder_width / 2, 0, cl2};
//+
Point(8) = {-bolder_width / 2, -bolder_width/ 2, 0, cl2};
//+
Point(9) = {dist_2bolder_x + bolder_width / 2,  bolder_width / 2 - dist_2bolder_y, 0, cl2};
//+
Point(10) = {dist_2bolder_x + bolder_width/ 2, -bolder_width/ 2 - dist_2bolder_y, 0, cl2};
//+
Point(11) = {dist_2bolder_x - bolder_width / 2,  bolder_width / 2 - dist_2bolder_y, 0, cl2};
//+
Point(12) = {dist_2bolder_x  - bolder_width / 2, -bolder_width/ 2 - dist_2bolder_y, 0, cl2};
//+
Point(13) = {dist_2bolder_x + bolder_width / 2, -bolder_width/ 2, 0, 1.0};
//+
Point(15) = {-bolder_width / 2, bolder_width / 2 - dist_2bolder_y, 0, 1.0};
//+
Point(16) = {-bolder_width / 2, -bolder_width/ 2 - dist_2bolder_y, 0, 1.0};
//+
Point(17) = {dist_2bolder_x - bolder_width / 2, -bolder_width/ 2, 0, 1.0};
//+
Point(18) = {dist_2bolder_x - bolder_width / 2,bolder_width / 2, 0, 1.0};
//+
Point(19) = {dist_2bolder_x + bolder_width / 2, w, 0, 1.0};
//+
Point(20) = {dist_2bolder_x + bolder_width / 2, -w, 0, 1.0};
//+
Point(21) = {-bolder_width / 2, -w, 0, 1.0};
//+
Point(22) = {-bolder_width / 2, w, 0, 1.0};
//+
Point(23) = {dist_2bolder_x - bolder_width / 2, -w, 0, 1.0};
//+
Point(24) = {dist_2bolder_x - bolder_width / 2, w, 0, 1.0};
//+
Point(25) = {-lus, -bolder_width / 2, 0, 1.0};
//+
Point(26) = {-lus, bolder_width / 2, 0, 1.0};
//+
Point(28) = {lds + dist_2bolder_x, bolder_width/ 2, 0, 1.0};
//+
Point(29) = {dist_2bolder_x + bolder_width / 2, bolder_width / 2, 0, 1.0};
//+
Point(30) = {lds + dist_2bolder_x, -bolder_width/ 2, 0, 1.0};
//+
Point(31) = {-lus, bolder_width / 2 - dist_2bolder_y, 0, 1.0};
//+
Point(32) = {-lus, -bolder_width/ 2 - dist_2bolder_y, 0, 1.0};
//+
Point(33) = {lds + dist_2bolder_x, bolder_width / 2 - dist_2bolder_y, 0, 1.0};
//+
Point(34) = {lds + dist_2bolder_x, -bolder_width/ 2 - dist_2bolder_y, 0, 1.0};
//+
Point(35) = {bolder_width / 2,  bolder_width / 2 - dist_2bolder_y, 0, cl2};
//+
Point(36) = {bolder_width/ 2, -bolder_width/ 2 - dist_2bolder_y, 0, cl2};
//+
Point(37) = {bolder_width / 2,  -w, 0, cl2};
//+
Point(38) = {bolder_width/ 2, w, 0, cl2};

//+
Line(1) = {4, 22};
//+
Line(2) = {22, 38};
//+
Line(3) = {38, 24};
//+
Line(4) = {24, 19};
//+
Line(5) = {19, 3};
//+
Line(6) = {3, 28};
//+
Line(7) = {28, 30};
//+
Line(8) = {30, 33};
//+
Line(9) = {33, 34};
//+
Line(10) = {34, 2};
//+
Line(11) = {2, 20};
//+
Line(12) = {20, 23};
//+
Line(13) = {23, 37};
//+
Line(14) = {37, 21};
//+
Line(15) = {21, 1};
//+
Line(16) = {1, 32};
//+
Line(17) = {32, 31};
//+
Line(18) = {31, 25};
//+
Line(19) = {25, 26};
//+
Line(20) = {26, 4};
//+
Line(21) = {7, 5};
//+
Line(22) = {5, 6};
//+
Line(23) = {6, 8};
//+
Line(24) = {8, 7};
//+
Line(25) = {11, 9};
//+
Line(26) = {9, 10};
//+
Line(27) = {10, 12};
//+
Line(28) = {12, 11};
//+
Line(29) = {26, 7};
//+
Line(30) = {8, 25};
//+
Line(31) = {31, 15};
//+
Line(32) = {15, 35};
//+
Line(33) = {35, 11};
//+
Line(34) = {12, 36};
//+
Line(35) = {36, 16};
//+
Line(36) = {16, 32};
//+
Line(37) = {16, 21};
//+
Line(38) = {36, 37};
//+
Line(39) = {36, 35};
//+
Line(40) = {15, 16};
//+
Line(41) = {15, 8};
//+
Line(42) = {6, 35};
//+
Line(43) = {12, 23};
//+
Line(44) = {10, 20};
//+
Line(45) = {9, 13};
//+
Line(46) = {13, 29};
//+
Line(47) = {29, 19};
//+
Line(48) = {24, 18};
//+
Line(49) = {18, 17};
//+
Line(50) = {17, 11};
//+
Line(51) = {6, 17};
//+
Line(52) = {17, 13};
//+
Line(53) = {5, 18};
//+
Line(54) = {18, 29};
//+
Line(55) = {7, 22};
//+
Line(56) = {5, 38};
//+
Line(57) = {29, 28};
//+
Line(58) = {13, 30};
//+
Line(59) = {10, 34};
//+
Line(60) = {9, 33};
//+
Curve Loop(1) = {1, -55, -29, 20};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {55, 2, -56, -21};
//+
Plane Surface(2) = {2};
//+
Curve Loop(3) = {56, 3, 48, -53};
//+
Plane Surface(3) = {3};
//+
Curve Loop(4) = {48, 54, 47, -4};
//+
Plane Surface(4) = {4};
//+
Curve Loop(5) = {5, 6, -57, 47};
//+
Plane Surface(5) = {5};
//+
Curve Loop(6) = {29, -24, 30, 19};
//+
Plane Surface(6) = {6};
//+
Curve Loop(7) = {53, 49, -51, -22};
//+
Plane Surface(7) = {7};
//+
Curve Loop(8) = {49, 52, 46, -54};
//+
Plane Surface(8) = {8};
//+
Curve Loop(9) = {57, 7, -58, 46};
//+
Plane Surface(9) = {9};
//+
Curve Loop(10) = {18, -30, -41, -31};
//+
Plane Surface(10) = {10};
//+
Curve Loop(11) = {41, -23, 42, -32};
//+
Plane Surface(11) = {11};
//+
Curve Loop(12) = {51, 50, -33, -42};
//+
Plane Surface(12) = {12};
//+
Curve Loop(13) = {50, 25, 45, -52};
//+
Plane Surface(13) = {13};
//+
Curve Loop(14) = {45, 58, 8, -60};
//+
Plane Surface(14) = {14};
//+
Curve Loop(15) = {17, 31, 40, 36};
//+
Plane Surface(15) = {15};
//+
Curve Loop(16) = {32, -39, 35, -40};
//+
Plane Surface(16) = {16};
//+
Curve Loop(17) = {33, -28, 34, 39};
//+
Plane Surface(17) = {17};
//+
Curve Loop(18) = {60, 9, -59, -26};
//+
Plane Surface(18) = {18};
//+
Curve Loop(19) = {16, -36, 37, 15};
//+
Plane Surface(19) = {19};
//+
Curve Loop(20) = {37, -14, -38, 35};
//+
Plane Surface(20) = {20};
//+
Curve Loop(21) = {38, -13, -43, 34};
//+
Plane Surface(21) = {21};
//+
Curve Loop(22) = {43, -12, -44, 27};
//+
Plane Surface(22) = {22};
//+
Curve Loop(23) = {59, 10, 11, -44};
//+
Plane Surface(23) = {23};

cell_num_uvl = 60;   // num of cell vertical lines up/down of cylinder
cell_num_dvl = 50; // num of cell over diagonal lines insde outer cylinder
cell_num_bldr = 30;  // num of cell around cylinder
cell_num_us = 240;
cell_num_ds = 150;
cell_num_mid = 80;     // middle blocks

grad_mid = 0.8;   // grading in diagonal lines
grad2 = 1.01;   // grading downstream horizonal lines
grad_us = 0.4;    // grading in upstream horizonla lines
grad_ds = 1.01;   // grading up/down vertical lines
grad_uvl = 0.4;    

Transfinite Line {20,55,56,47,48,6} = cell_num_uvl Using Bump grad_uvl;  // up vertical lines
Transfinite Line {16,37,38,43,44,10} = cell_num_dvl;  // down vertical lines
Transfinite Line {1,29,30,31,36,15}= cell_num_us Using Bump grad_us; // upstream horizonla lines
Transfinite Line {3,53,51,33,34,13} = cell_num_mid Using Bump grad_mid; // blocks between two bolders
Transfinite Line {18,41,42,45,50,8} = cell_num_bldr; // end lines in cross in middle 
Transfinite Line {21,22,23,24,25,26,27,28} = cell_num_bldr;  // bolders
Transfinite Line {19,17,32,35,39,40,14,2,4,46,49,52,54,12,7,9} = cell_num_bldr; // line parallel to bolders
Transfinite Line {5,57,58,59,60,-11} = cell_num_ds Using Progression grad_ds;  // downstream vertical lines


layer_botom = 12;
layer_up = 30;
layer_middle = 8;

Extrude {0, 0, extr_small_bolder} { Surface{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}; Layers{layer_botom}; Recombine;}

//+
Curve Loop(24) = {439, -327, 415, -527};
//+
Plane Surface(567) = {24};

Extrude {0, 0, extr - extr_small_bolder} { Surface{82,192,280,390,478,104,302,412,500,522,434,324,214,126,544,567,346,236,148,566,456,368,258,170};Layers{{5,8,20},{0.1,0.3,1.0}}; Recombine;}


Transfinite Surface{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,567,82,192,280,104,302,412,500,522,434,324,214,126,544,567,346,236,148,566,456,368,258,170};
Recombine Surface {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,567,82,192,280,104,302,412,500,522,434,324,214,126,544,567,346,236,148,566,456,368,258,170};



Physical Surface("inlet") = {588,610,620,642,664,81,191,167,377,465,267};
Physical Surface("outlet") = {161,249,363,447,557,1086,1064,1046,1020,998};
Physical Surface("big_bolder") = {103,183,213,293,602,698,712,852};
Physical Surface("small_bolder") = {543,455,337,567,425};
Physical Surface("wall") ={561,1002,535,513,888,778,513,491,756,477,676,69,576,95,690,866,117,984,147,157,1082};
Physical Surface("bottom") ={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
Physical Surface("atmosphere") ={1095,1073,1051,1029,1007,985,963,941,919,897,787,809,831,853,875,765,743,721,699,677,655,633,611,589};

Physical Surface("symmetric") = {82,192,280,390,478,104,302,412,500,522,434,324,214,126,544,346,236,148,566,456,368,258,170,389,279,187,77,473,407,495,385,301,403,275,297,99,73,429,319,209,125,517,539,341,315,227,205,139,231,143,121,165,253,367,451,654,632,606,584,672,760,738,650,720,628,716,580,694,786,796,818,840,782,892,896,800,910,906,822,936,940,844,962,958,870,980,994,1016,1042,1060,734};
Physical Volume("internalField") ={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47};


