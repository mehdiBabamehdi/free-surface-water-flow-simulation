
cl1 = 6;
cl2 = .03;
cl3 = 10;

scale = 1.0e-2;         // convert to cm
bolder_width = 6 * scale;
small_bolder_height = 6 * scale;
dist_2bolder = 7 * bolder_width;            // distance between center of two bolders
out_rad = dist_2bolder / 2;
extr_small_bolder = 6 * scale;        // length of domain prependicular to surface (Z direction)
extr = 30 * scale;                    // length of domain prependicular to surface (Z direction)
lus = bolder_width * 40;              // length of upstream
lds = bolder_width * 30;             // length of downstream
w = 40 * scale;                                              // Width of domain


/* (0,0,0) is the center of big bolder */
// Exterior (bounding box) of mesh
Point(1) = {-lus, -w, 0, cl1};
//+
Point(2) = { lds + dist_2bolder, -w, 0, cl3};
//+
Point(3) = { lds + dist_2bolder,  w, 0, cl3};
//+
Point(4) = {-lus,  w, 0, cl1};
//+

// Circle & surrounding structured-quad region
//+
Point(5) = {bolder_width / 2,  bolder_width / 2, 0, cl2};
//+
Point(6) = {bolder_width/ 2, -bolder_width/ 2, 0, cl2};
//+
Point(7) = {-bolder_width / 2,  bolder_width / 2, 0, cl2};
//+
Point(8) = {-bolder_width / 2, -bolder_width/ 2, 0, cl2};
//+
Point(9) = {dist_2bolder + bolder_width / 2,  bolder_width / 2, 0, cl2};
//+
Point(10) = {dist_2bolder + bolder_width/ 2, -bolder_width/ 2, 0, cl2};
//+
Point(11) = {dist_2bolder - bolder_width / 2,  bolder_width / 2, 0, cl2};
//+
Point(12) = {dist_2bolder  - bolder_width / 2, -bolder_width/ 2, 0, cl2};
//+
Point(13) = {dist_2bolder + out_rad, out_rad, 0, 1.0};
//+
Point(14) = {dist_2bolder + out_rad, -out_rad, 0, 1.0};
//+
Point(15) = {-out_rad, -out_rad, 0, 1.0};
//+
Point(16) = {-out_rad, out_rad, 0, 1.0};
//+
Point(17) = {bolder_width / 2, -out_rad, 0, 1.0};
//+
Point(18) = {bolder_width / 2, out_rad, 0, 1.0};
//+
Point(19) = {dist_2bolder + out_rad, w, 0, 1.0};
//+
Point(20) = {dist_2bolder + out_rad, -w, 0, 1.0};
//+
Point(21) = {-out_rad, -w, 0, 1.0};
//+
Point(22) = {-out_rad, w, 0, 1.0};
//+
Point(23) = {bolder_width / 2, -w, 0, 1.0};
//+
Point(24) = {bolder_width / 2, w, 0, 1.0};
//+
Point(25) = {-lus, -out_rad, 0, 1.0};
//+
Point(26) = {-lus, out_rad, 0, 1.0};
//+
Point(27) = {lds + dist_2bolder, -out_rad, 0, 1.0};
//+
Point(28) = {lds + dist_2bolder, out_rad, 0, 1.0};
//+
Point(29) = {dist_2bolder - bolder_width / 2, -out_rad, 0, 1.0};
//+
Point(30) = {dist_2bolder - bolder_width / 2, out_rad, 0, 1.0};
//+
Point(31) = {dist_2bolder - bolder_width / 2, -w, 0, 1.0};
//+
Point(32) = {dist_2bolder - bolder_width / 2, w, 0, 1.0};
//+
Line(3) = {7, 5};
//+
Line(4) = {5, 6};
//+
Line(5) = {6, 8};
//+
Line(6) = {8, 7};
//+
Line(7) = {11, 9};
//+
Line(8) = {9, 10};
//+
Line(9) = {10, 12};
//+
Line(10) = {12, 11};
//+
Line(11) = {16, 15};
//+
Line(12) = {13, 14};
//+
Line(13) = {8, 15};
//+
Line(14) = {16, 7};
//+
Line(15) = {13, 9};
//+
Line(16) = {14, 10};
//+
Line(17) = {16, 18};
//+
Line(18) = {18, 5};
//+
Line(19) = {15, 17};
//+
Line(20) = {6, 17};
//+
Line(22) = {12, 29};
//+
Line(24) = {30, 11};
//+
Line(25) = {5, 11};
//+
Line(26) = {12, 6};
//+
Line(27) = {4, 26};
//+
Line(28) = {26, 25};
//+
Line(29) = {25, 1};
//+
Line(30) = {1, 21};
//+
Line(31) = {21, 23};
//+
Line(33) = {20, 2};
//+
Line(34) = {2, 27};
//+
Line(35) = {27, 28};
//+
Line(36) = {28, 3};
//+
Line(37) = {3, 19};
//+
Line(39) = {24, 22};
//+
Line(40) = {22, 4};
//+
Line(41) = {26, 16};
//+
Line(42) = {16, 22};
//+
Line(43) = {15, 25};
//+
Line(44) = {15, 21};
//+
Line(45) = {17, 23};
//+
Line(46) = {14, 20};
//+
Line(47) = {13, 19};
//+
Line(48) = {24, 18};
//+
Line(49) = {13, 28};
//+
Line(50) = {14, 27};
//+
Line(51) = {19, 32};
//+
Line(52) = {32, 24};
//+
Line(53) = {18, 30};
//+
Line(54) = {30, 32};
//+
Line(55) = {30, 13};
//+
Line(56) = {29, 17};
//+
Line(57) = {23, 31};
//+
Line(58) = {31, 29};
//+
Line(59) = {29, 14};
//+
Line(60) = {20, 31};
//+

Curve Loop(1) = {41, 42, 40, 27};
//+
Plane Surface(1) = {1};
//+
Curve Loop(2) = {41, 11, 43, -28};
//+
Plane Surface(2) = {2};
//+
Curve Loop(3) = {43, 29, 30, -44};
//+
Plane Surface(3) = {3};
//+
Curve Loop(4) = {11, -13, 6, -14};
//+
Plane Surface(4) = {4};
//+
Curve Loop(5) = {39, -42, 17, -48};
//+
Plane Surface(5) = {5};
//+
Curve Loop(6) = {18, -3, -14, 17};
//+
Surface(6) = {6};
//+
Curve Loop(7) = {13, 19, -20, 5};
//+
Surface(7) = {7};
//+
Curve Loop(9) = {25, -10, 26, -4};
//+
Plane Surface(9) = {9};
//+
Curve Loop(15) = {15, 8, -16, -12};
//+
Plane Surface(15) = {15};
//+
Curve Loop(17) = {19, 45, -31, -44};
//+
Plane Surface(17) = {17};
//+
Curve Loop(18) = {37, -47, 49, 36};
//+
Plane Surface(18) = {18};
//+
Curve Loop(19) = {49, -35, -50, -12};
//+
Plane Surface(19) = {19};
//+
Curve Loop(20) = {50, -34, -33, -46};
//+
Plane Surface(20) = {20};
//+
Curve Loop(21) = {54, 52, 48, 53};
//+
Plane Surface(21) = {21};
//+
Curve Loop(22) = {51, -54, 55, 47};
//+
Plane Surface(22) = {22};
//+
Curve Loop(23) = {55, 15, -7, -24};
//+
Surface(23) = {23};
//+
Curve Loop(24) = {53, 24, -25, -18};
//+
Plane Surface(24) = {24};
//+
Curve Loop(25) = {26, 20, -56, -22};
//+
Plane Surface(25) = {25};
//+
Curve Loop(26) = {56, 45, 57, 58};
//+
Plane Surface(26) = {26};
//+
Curve Loop(27) = {59, 46, 60, 58};
//+
Plane Surface(27) = {27};
//+
Curve Loop(28) = {9, 22, 59, 16};
//+
Surface(28) = {28};

layer_botom = 12;
layer_up = 30;
layer_middle = 8;

cell_num_ud = 30;   // num of cell vertical lines up/down of cylinder
cell_num_diag = 60; // num of cell over diagonal lines insde outer cylinder
cell_num_bldr = 40;  // num of cell around cylinder
cell_num_us = 200;
cell_num_ds = 120;
cell_num_btwn = cell_num_diag * 2; // num of cell between tow cylinders

grad1 = 1.02;   // grading in diagonal lines
grad2 = 1.01;   // grading downstream horizonal lines
grad3 = 0.7;    // grading in upstream horizonla lines
grad4 = 0.7;   // grading up/down vertical lines
grad5 = 0.6;   // grading between two bolders

Transfinite Line {27,42,48,54,47,36,29,44,45,58,46,34} = cell_num_ud Using Bump grad4;  // up/down vertical lines
Transfinite Line {40,41,43,30}= cell_num_us Using Bump grad3; // upstream horizonla lines
Transfinite Line {33,50,49,-37} = cell_num_ds Using Progression grad2; // upstream horizontal lines
Transfinite Line {39,17,19,31,51,55,59,60} = cell_num_bldr; // horizonal line up and down the bolders 
Transfinite Line {3,4,5,6,7,8,9,10} = cell_num_bldr;  // bolder
Transfinite Line {28,11,12,35} = cell_num_bldr;  // bolder
Transfinite Line {52,53,26,25,56,57} = cell_num_btwn Using Bump grad5;  // between two bolders
Transfinite Line {13,-14,-18,20,22,-24,-15,-16} = cell_num_diag Using Progression grad1; // diagonals

Extrude {0, 0, extr_small_bolder} { Surface{1,2,3,4,5,6,7,9,15,17,18,19,20,21,22,23,24,25,26,27,28}; Layers{layer_botom}; Recombine;}

//+
Curve Loop(29) = {394, 217, -502, -239};
//+
Plane Surface(523) = {29};
//+

Extrude {0, 0, extr - extr_small_bolder} { Surface{302,324,346,390,258,412,500,522,368,434,236,456,478,523,214,280,148,192,170,126,104,82};Layers{{5,8,20},{0.1,0.3,1.0}}; Recombine;}

Transfinite Surface{1,2,3,4,5,6,7,9,15,17,18,19,20,21,22,23,24,25,26,27,28,29,190,168,146,300,278,256,234,124,212,102,80,58,301,523};
Recombine Surface {1,2,3,4,5,6,7,9,15,17,18,19,20,21,22,23,24,25,26,27,28,29,190,168,146,300,278,256,234,124,212,102,80,58,301,523};


Physical Surface("outlet") = {301,315,337,544,558,580};
Physical Surface("inlet") = {81,103,117,1006,984,954};
Physical Surface("big_bolder") = {183,235,213,143,910,892,764,852};
Physical Surface("small_bolder") = {407,249,227,509,523};
Physical Surface("wall") ={289,532,341,584,377,598,712,359,157,928,77,1002,495,672,473,804,275,870,121,958};
Physical Surface("bottom") ={1,2,3,4,5,6,7,9,15,17,18,19,20,21,22,23,24,25,26,27,28};
Physical Surface("atmosphere") ={589,567,545,611,633,655,677,699,721,743,765,787,809,831,919,941,853,897,875,1007,985,963};

Physical Surface("symmetric") = {302,324,346,390,258,412,500,522,368,434,236,456,478,214,280,148,192,170,126,104,82,297,540,319,564,293,536,257,566,345,588,245,620,253,628,487,664,477,676,455,690,411,654,355,602,367,720,223,738,231,760,451,782,271,800,209,778,742,179,169,716,165,918,73,932,174,896,884,95,139,840,205,844,125,874,822,826,650,624,686,756,818,69,972,99,950,562,606,147,385};
Physical Volume("internalField") ={1,2,3,4,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43};






