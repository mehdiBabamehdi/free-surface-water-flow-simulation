
#!/usr/bin/env python3
'''
'@name   dataFetchLib.py
'@info   The library includes the classes developed to fetch all the data generated in 
'          experimental test or numerical simulations e.g forces, height
'@author Mehdi Baba Mehdi
'@ver    1.0

'''

# Import libraries
from  os import walk,path, utime
from re import search
import matplotlib as plt
import sys
from math import sqrt,pow,log,pi
import matplotlib.pyplot as plt
import numpy as np
import csv

'''
' @name listClass
' @info The class is a data structure contains lists to store the data
'       needed to be ploted
'''
class listClass:
    def __init__(self):
        self.time = np.array([])
        self.height_1 = np.array([])
        self.height_2 = np.array([])
        self.height_3 = np.array([])
        self.force_big_bolder = np.array([])
        self.force_small_bolder = np.array([])
        self.pres_force = np.array([])
        self.pres_forceX = np.array([])
        self.pres_forceY = np.array([])
        self.pres_forceZ = np.array([])
        self.vis_force = np.array([])
        self.vis_forceX = np.array([])
        self.vis_forceY = np.array([])
        self.vis_forceZ = np.array([])
        self.vis_force = np.array([])
        self.pres_force = np.array([])
        self.force = np.array([])
        self.Cd = np.array([])
   

                 
'''
' @name  readExpreimentalHeight
' @info  This class  read the values (height) of experiment  and store it in an object of data class
' @input  flow_rate_: flow rate in litter/second
'        
'''
class readExpreimentalHeight():
    def __init__(self, flow_rate_):
        self.listClass = listClass()
        self.flow_rate = flow_rate_
        self.canal_width = 80
           
    def run(self):
        
        if  len(sys.argv) != 2 :
           print("No directory is specified")
           print(str(len(sys.argv)))
           exit()
      
        myPath = sys.argv[1]

        with open(myPath) as src:
             lines = src.readlines()

             for line in lines:
                 parts = line.split()
                 count = 0
                 for item in parts:
                     parts[count] = parts[count].replace(',','')
                     count = count + 1
                 self.listClass.time = np.append(self.listClass.time, float(parts[0]))
                
                 self.listClass.height_1 = np.append(self.listClass.height_1, float(parts[1]))
               
                 self.listClass.height_2 = np.append(self.listClass.height_2, float(parts[2]))
               
                 self.listClass.height_3 = np.append(self.listClass.height_3, float(parts[3]))
             print(str(self.listClass.time[-1]) + "  " + str( self.listClass.height_1[-1]) +  "  " + str( self.listClass.height_2[-1]) + "  " + str( self.listClass.height_3[-1])     )
        print("Mean vlaue of first sensor = " + str(np.mean( self.listClass.height_1)) + " mm")
        print("Var vlaue of first sensor = " + str(np.var( self.listClass.height_1)) + " mm")
        print("Mean vlaue of second sensor = " + str(np.mean( self.listClass.height_2))+ " mm")
        print("Mean vlaue of third sensor = " + str(np.mean( self.listClass.height_3)) + " mm")
        print("Inlet velocity = " + str(self.flow_rate / np.mean( self.listClass.height_1) / self.canal_width * 100) + " m/s")
                       
     
                  
'''
' @name  readExpreimentalForce
' @info   This class  read the values (force) of experiment  and store it in  an object of data class
'        
'''
class readExpreimentalForce():
    def __init__(self):
        self.listClass = listClass()
           
    def run(self):
        
        if  len(sys.argv) != 2 :
           print("No directory is specified")
           exit()

        myPath = sys.argv[1]
        force_coeff = 30.0 * 1.0e-3
               
        with open(myPath) as src:
             lines = src.readlines()

             for line in lines:
                 parts = line.split()
                 count = 0

                 for item in parts:
                     parts[count] = parts[count].replace(',','')
                     count = count + 1 
                     
                 self.listClass.time = np.append(self.listClass.time, float(parts[1]))
                 self.listClass.force_big_bolder = np.append(self.listClass.force_big_bolder, float(parts[1]) * force_coeff)
                 self.listClass.force_small_bolder = np.append(self.listClass.force_small_bolder, float(parts[2]) * force_coeff)
 
        print("Mean vlaue of force on big bolder= " + str(np.mean( self.listClass.force_big_bolder[100:])) + " N")   
        print("Mean vlaue of force on small bolder= " + str(np.mean( self.listClass.force_small_bolder[100:])) + " N")   
        print("Var vlaue of force on big bolder= " + str(np.var( self.listClass.force_big_bolder[100:])) + " N")   
        print("Var vlaue of force on small bolder= " + str(np.var( self.listClass.force_small_bolder[100:])) + " N")   
     
                 
'''
' @name  readNumericalForce
' @info  This class  read the values (force) of numerical simulation and calculates Cd values
'        and store value of parameters read to an object of data class
' @input inlet_velocity_ : fluid flow velocity at inlet
'        bolder_height_ : height of bolder in channel
'        
'''
class readNumericalForce():
    def __init__(self, inlet_velocity_, bolder_height_):
        self.listClass = listClass()
        self.inlet_velocity = inlet_velocity_
        self.bolder_height = bolder_height_
        self.bolder_width = 6e-2
        self.rho = 998.8
           
    def run(self):
        
        if  len(sys.argv) != 3 :
           print("No directory is specified")
           exit()
           
        myPath = sys.argv[1]
        outputFile = sys.argv[2]
                
        print("%10s\t%10s\t%10s" %( "Time", "Force" , "Cd"))
        cd_coeff = 2.0 / (self.rho * self.bolder_width * self.bolder_height * pow(self.inlet_velocity,2))
        with open(myPath) as src:
             lines = src.readlines()[4:]

             for line in lines:
                 value = 0
                 parts = line.split()
                 count = 0

                 for item in parts:
                     parts[count] = parts[count].replace(')','')
                     parts[count] = parts[count].replace('(','')
                     parts[count] = parts[count].replace(',','')
                     count = count + 1 
                 
                 self.listClass.time = np.append(self.listClass.time, float(parts[0]))
             
                 self.listClass.pres_forceX = np.append(self.listClass.pres_forceX, float(parts[1]))

                 self.listClass.pres_forceY = np.append(self.listClass.pres_forceY, float(parts[2]))

                 self.listClass.pres_forceZ = np.append(self.listClass.pres_forceZ, float(parts[3]))

                 self.listClass.vis_forceX = np.append(self.listClass.vis_forceX, float(parts[4]))

                 self.listClass.vis_forceY = np.append(self.listClass.vis_forceY, float(parts[5]))

                 self.listClass.vis_forceZ = np.append(self.listClass.vis_forceZ, float(parts[6]))

                 pres_force = sqrt(pow(self.listClass.pres_forceX[-1], 2) +
                                   pow(self.listClass.pres_forceY[-1], 2) +
                                   pow(self.listClass.pres_forceZ[-1], 2))

                 vis_force = sqrt(pow(self.listClass.vis_forceX[-1], 2) +
                                  pow(self.listClass.vis_forceY[-1], 2) +
                                  pow(self.listClass.vis_forceZ[-1], 2))

                 force = sqrt(pow(self.listClass.pres_forceX[-1] + self.listClass.vis_forceX[-1], 2) +
                              pow(self.listClass.pres_forceY[-1] + self.listClass.vis_forceY[-1], 2) +
                              pow(self.listClass.pres_forceZ[-1] + self.listClass.vis_forceZ[-1], 2))

                 self.listClass.vis_force = np.append(self.listClass.vis_force,   vis_force)
                 self.listClass.pres_force = np.append(self.listClass.pres_force, pres_force)
                 self.listClass.force = np.append(self.listClass.force, force)
                 
                 Cd = force * cd_coeff
                 self.listClass.Cd = np.append(self.listClass.Cd, Cd)

                 print("%6.4f\t%10.7f\t%10.7f" %(self.listClass.time[-1], self.listClass.force[-1], self.listClass.Cd[-1]))
             print("numerical force = " + str(np.mean(self.listClass.force[-40:-1])))
             plt.plot(self.listClass.time,self.listClass.vis_force,'o',label="viscous force")
             plt.plot(self.listClass.time,self.listClass.pres_force,'*',label="pressure force")
             plt.plot(self.listClass.time,self.listClass.force,'-',label="total force")
             plt.legend()
             plt.xlabel('Time(sec)')
             plt.ylabel('Force(N)')
             plt.savefig("./figures/" + str(outputFile) + ".png")
             plt.show()
             plt.close()

'''
' @name  readNumericalalHeight
' @info  This class  read the values (height) of numerical simulation and store it in an object of data class
'        
'''
class readNumericalHeight():
    def __init__(self):
        pass
           
    def run(self):
        
        if  len(sys.argv) != 2 :
           print("No directory is specified")
           print(str(len(sys.argv)))
           exit()
      
        myPath = sys.argv[1]

        with open(myPath, "r") as csv_file:
             csv_reader = csv.reader(csv_file, delimiter=',')
             old_height = 0
             old_alpha = 1
             row1 = next(csv_reader)

             for parts in csv_reader:
      
                 new_height = float(parts[0])
                
                 new_alpha = float(parts[1])
                 print(str(new_height)  + "   " + str(new_alpha))  
                 if (new_alpha <= 0.5):
                     height_50 = (old_height - new_height) / (old_alpha - new_alpha) * (0.5 - old_alpha) + old_height
                     print("Height of water-air interface = " + str(height_50))
                     break

                 old_alpha = new_alpha
                 old_height = new_height


