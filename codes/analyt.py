



#!/usr/bin/env python3


import matplotlib.pyplot as plt
from math import sin,pi,pow


def calculateForce(c, wd,k, hk):
  b = 0.8 
  bk = b * pow(2,0.5) * sin(0.01+pi/4)
  rho = 998.0
  corr = 1.2
  flow_rate = [0.1,1,1.5,3,5,10,15,20,25,30,35,40,45,50]

  f = []
  for q in flow_rate:
    if (wd <= hk):
      f.append(c * rho * pow(q*0.001,2) / (2.0  * 9.81* pow(b,2) * wd * corr) *bk)
    if (wd > hk):
      f.append(c * rho * pow(q*0.001,2) / (2.0 * 9.81 * pow(b,2) * hk * corr) *bk + 0.5 * rho * wd * 0.1)
  
  return f

b_exp_f = [0.49811, 1.1400, 1.9794, 2.921, 3.7352]
b_exp_h = [19.5318206, 28.896174912, 38.3820044, 46.914618, 52.1303800]
s_exp_f = [0.42137, 1.04292, 1.6089, 2.0801, 2.46617]
s_exp_h = [19.805233, 28.8197, 38.224, 46.9708, 52.054]
nb_exp_h = [19.43, 29.09, 38.195, 46.46, 52.715]
nb_exp_h_err = [0.1660 , 0.2265 , 0.5721 , 0.8316 , 1.05676]
s_exp_f_err =  [0.005, 0.03, 0.06,0.06,0.07]
s_exp_h_err = [0.178,0.3,0.56,1.3,1.34]
b_exp_f_err = [0.003, 0.02,0.07,0.102,0.202]
b_exp_h_err = [ 0.169, 0.23,0.56,1.093,1.14]


qex = [10,20,30,40,49.2]
tol = 1.0e-6
h_old = 1.0
flow_rate = [0.1,1,1.5,3,5,10,15,20,25,30,35,40,45,50]
strikler = [0.5, 1, 2, 3, 4]
cd = [0.5, 1, 1.25, 1.5, 1.75, 2]
b = 0.8
I = 0.01
p1 = plt.figure()
p2 = plt.figure()
p3 = plt.figure()
plot1 = p1.add_subplot(1,1,1)
plot2 = p2.add_subplot(1,1,1)
plot3 = p3.add_subplot(1,1,1)
for k in strikler:
  wd = []
  vel = []
  print(str(26.0/pow(k*0.001,1/6)))
  for q in flow_rate:
    while True:
      h_new = q * 0.001 / (26.0 / pow(k*0.001, 1/6) * pow((b * h_old)/(b+2*h_old),2/3) * pow(I,0.5) * b)
      if (abs(h_old - h_new) < tol):
        wd.append(h_new)
        vel.append(q/b/h_new)
        
        break
      else:
        h_old = h_new
  if ( k == 1 or k == 3):
    for c in cd:
      f = calculateForce(c, h_new, k,0.06)
      print(str(k) + str(c))
      plot3.plot(flow_rate, f, label= r"$k_m = $" + str(k) + r", $C_D =$ " + str(c))
      if c == 2.0:
        fan = f 
  plot1.plot(flow_rate, wd, label = r"$k_m = $" + str(k))
  plot2.plot(flow_rate, vel, label = r"$k_m = $" + str(k))
  if (k == 0.5):
    han = []
    for i in wd:
      han.append(i*1000)
    
plot1.legend()
plot1.set_xlabel('Q(liter/sec)')
plot1.set_ylabel('water depth (m)')
plot1.set_title("discharge curve")
p1.savefig("./figures/disch.png")
p1.show()

plt.close()
plot2.legend()
plot2.set_xlabel('Q(liter/sec)')
plot2.set_ylabel('velocity (m/s)')
plot2.set_title("Velocity of fluid flow")
p2.savefig("./figures/vel.png")
p2.show()

plt.close()

plot3.legend()
plot3.set_xlabel('Q(liter/sec)')
plot3.set_ylabel('Force (N)')
plot3.set_title("Force acts on bolder")
p3.savefig("./figures/f.png")
p3.show()
plt.close()

q = flow_rate
plt.plot(q,han,'-',label="analytical data")
plt.errorbar(qex,nb_exp_h,yerr=nb_exp_h_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Water depth(mm)')
plt.title("Water depth in channel without bolder")
plt.savefig("./figures/nbDepth_an.png")
plt.show()
plt.close()



plt.plot(q,fan,label="analitical data")
plt.errorbar(qex,b_exp_f,yerr=b_exp_f_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Force(N)')
plt.title("Total force acts on big bolder")
plt.savefig("./figures/bigForce_an.png")
plt.show()
plt.close()

plt.plot(q,fan,'-',label="analytical data")
plt.errorbar(qex,s_exp_f,yerr=s_exp_f_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Force(N)')
plt.title("Total force acts on small bolder")
plt.savefig("./figures/smallForce_an.png")
plt.show()
plt.close()

plt.plot(q,han,'-',label="analytical data")
plt.errorbar(qex,b_exp_h,yerr=b_exp_h_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Water depth(mm)')
plt.title("Water depth in big bolder")
plt.savefig("./figures/bigDepth_an.png")
plt.show()
plt.close()


plt.plot(q,han,'-',label="analyticaldata")
plt.errorbar(qex,s_exp_h,yerr=s_exp_h_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Water depth(mm)')
plt.title("Water depth in small bolder")
plt.savefig("./figures/smallDepth_an.png")
plt.show()
plt.close()

