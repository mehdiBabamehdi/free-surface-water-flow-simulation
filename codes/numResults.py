
#!/usr/bin/env python3


import matplotlib.pyplot as plt

ks = [5e-4, 1e-3, 2e-3, 3e-3]
b_f = [4.8090, 4.5286, 4.2370, 4.02151]
b_h = [41.1345,43.8530 ,45.8923 , 48.162]
exp_f = 3.7352
exp_h = 52.13038

q0 = [10,30,49.2]
b_num_f_3 = [0.46642,2.13899, 4.02151]
b_num_h_3 = [29.6569, 37.261,  48.162]
b_num_f_1 = [0.51103, 2.3904, 4.5286]
b_num_h_1= [20.666, 31.9328, 43.8530]

b_exp_f0 = [0.49811, 1.9794, 3.7352]
b_exp_h0 = [19.5318206, 38.3820044, 52.1303800]



for i in range(4):
  b_f[i] = abs(b_f[i] - exp_f) / exp_f * 100
  #print(str(b_f[i]))


for i in range(4):
  b_h[i] = abs(b_h[i] - exp_h) / exp_h * 100
  #print(str(b_h[i]))


q = [10,20,30,40,49.2]

nb_exp_h = [19.43, 29.09, 38.195, 46.46, 52.715]
nb_exp_h_err = [0.1660 , 0.2265 , 0.5721 , 0.8316 , 1.05676]
nb_num_h = [ 25.171, 35.85, 43.804, 49.2305 ,53.80]
b_num_f = [0.46642,1.2814, 2.13899, 2.9959, 4.02151]
b_num_h = [29.6569, 40.788, 37.261, 42.4431, 48.162]

b_exp_f = [0.49811, 1.1400, 1.9794, 2.921, 3.7352]
b_exp_h = [19.5318206, 28.896174912, 38.3820044, 46.914618, 52.1303800]
b_exp_f_err = [0.003, 0.02,0.07,0.102,0.202]
b_exp_h_err = [ 0.169, 0.23,0.56,1.093,1.14]

s_num_f = [0.456322, 1.2162, 1.66312, 1.99885, 2.49103]
s_num_h = [29.610, 37.564, 36.4765, 42.4859, 48.1543]

s_exp_f = [0.42137, 1.04292, 1.6089, 2.0801, 2.46617]
s_exp_h = [19.805233, 28.8197, 38.224, 46.9708, 52.054]
s_exp_f_err =  [0.005, 0.03, 0.06,0.06,0.07]
s_exp_h_err = [0.178,0.3,0.56,1.3,1.34]

b_e_f = []
b_e_h = []
s_e_f = []
s_e_h = []
for i in range(5):
  b_e_f.append(abs(b_num_f[i] - b_exp_f[i]) / b_exp_f[i] * 100)
  b_e_h.append(abs(b_num_h[i] - b_exp_h[i]) / b_exp_h[i] * 100)
  s_e_f.append(abs(s_num_f[i] - s_exp_f[i]) / s_exp_f[i] * 100)
  s_e_h.append(abs(s_num_h[i] - s_exp_h[i]) / s_exp_h[i] * 100)
  print("big  " + str(b_e_f[i]) + "  " + str(b_e_h[i]))
  print("small  " + str(s_e_f[i]) + "  " + str(s_e_h[i]))

q1 = [10,30,49.3]

sbi_b_num_f = [0.43698, 2.1363, 4.0352]
sbi_s_num_f = [0.2455, 1.273, 1.4735]
sbi_num_h = [30.00, 36.4718, 48.226]

sbi_b_exp_f = [0.5434668, 2.0300, 3.746325]
sbi_s_exp_f = [0.348827, 1.225327, 1.471248]
sbi_exp_h = [18.19, 37.04243, 50.15632]
sbi_b_exp_f_err= [0.002,0.09,0.18]
sbi_s_exp_f_err = [0.001,0.04,0.15]
sbi_exp_h_err = [0.16,0.56,1.54]


sbs_b_num_f = [0.38252, 2.1573, 4.0837]
sbs_s_num_f = [0.50392, 1.63378, 2.2859]
sbs_num_h = [30.3671, 36.5176, 47.8574]

sbs_b_exp_f = [0.552248, 1.978554, 3.8157876]
sbs_s_exp_f = [0.50621, 1.549488, 2.315473]
sbs_exp_h = [ 19.805233, 38.224, 52.054]
sbs_b_exp_f_err= [0.002,0.074,0.185]
sbs_s_exp_f_err = [0.003,0.06,0.07]
sbs_exp_h_err = [0.164,0.56,1.14]


i_b_e_f = []
s_b_e_f = []
i_s_e_f = []
s_s_e_f = []
i_e_h = []
st_e_h = []
for i in range(3):
  i_b_e_f.append(abs(sbi_b_num_f[i] - sbi_b_exp_f[i]) / sbi_b_exp_f[i] * 100)
  s_b_e_f.append(abs(sbs_b_num_f[i] - sbs_b_exp_f[i]) / sbs_b_exp_f[i] * 100)
  i_s_e_f.append(abs(sbi_s_num_f[i] - sbi_s_exp_f[i]) / sbi_s_exp_f[i] * 100)
  s_s_e_f.append(abs(sbs_s_num_f[i] - sbs_s_exp_f[i]) / sbs_s_exp_f[i] * 100)
  i_e_h.append(abs(sbi_num_h[i] - sbi_exp_h[i]) / sbi_exp_h[i] * 100)
  st_e_h.append(abs(sbs_num_h[i] - sbs_exp_h[i]) / sbs_exp_h[i] * 100)
  print("inline  " + str(i_b_e_f[i]) + "  " + str(i_s_e_f[i]) + "  " +  str(i_e_h[i]))
  print("stag  " + str(s_b_e_f[i]) + "  " + str(s_s_e_f[i]) + "  " +  str(st_e_h[i]))




plt.plot(ks,b_f,label="Error in force")
plt.plot(ks,b_h,label="Error in water depth")
plt.legend()
plt.xlabel('Ks(m)')
plt.ylabel('Error (%)')
plt.title("Study the impact of Ks on total force and water depth")
plt.savefig("./figures/ks.png")
plt.show()
plt.close()

plt.plot(q0,b_num_f_1,label="Ks = 0.001")
plt.plot(q0,b_num_f_3,label="Ks = 0.003")
plt.plot(q0,b_exp_f0,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Force(N)')
plt.title("Total force acts on big bolder")
plt.savefig("./figures/bigFKs.png")
plt.show()
plt.close()

plt.plot(q0,b_num_h_1,label="Ks = 0.001")
plt.plot(q0,b_num_h_3,label="Ks = 0.003")
plt.plot(q0,b_exp_h0,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Water depth(mm)')
plt.title("Water depth in big bolder")
plt.savefig("./figures/bigHKs.png")
plt.show()
plt.close()

plt.plot(q,nb_num_h,'-',label="numerical data")
plt.errorbar(q,nb_exp_h,yerr=nb_exp_h_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Water depth(mm)')
plt.title("Water depth when without bolder in the channel")
plt.savefig("./figures/nbDepth.png")
plt.show()
plt.close()



plt.plot(q,b_num_f,label="numerical data")
plt.errorbar(q,b_exp_f,yerr=b_exp_f_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Force(N)')
plt.title("Total force acts on big bolder")
plt.savefig("./figures/bigForce.png")
plt.show()
plt.close()

plt.plot(q,s_num_f,'-',label="numerical data")
plt.errorbar(q,s_exp_f,yerr=s_exp_f_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Force(N)')
plt.title("Total force acts on small bolder")
plt.savefig("./figures/smallForce.png")
plt.show()
plt.close()

plt.plot(q,b_num_h,'-',label="numerical data")
plt.errorbar(q,b_exp_h,yerr=b_exp_h_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Water depth(mm)')
plt.title("Water depth in big bolder")
plt.savefig("./figures/bigDepth.png")
plt.show()
plt.close()

plt.plot(q,s_num_h,'-',label="numerical data")
plt.errorbar(q,s_exp_h,yerr=s_exp_h_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Water depth(mm)')
plt.title("Water depth in small bolder")
plt.savefig("./figures/smallDepth.png")
plt.show()
plt.close()

plt.plot(q,b_e_f,label="Error in force")
plt.plot(q,b_e_h,label="Error in water depth")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Error (%)')
plt.title("Error in total force and water depth in big bolder")
plt.savefig("./figures/b_e.png")
plt.show()
plt.close()

plt.plot(q,s_e_f,label="Error in force")
plt.plot(q,s_e_h,label="Error in water depth")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Error (%)')
plt.title("Error in total force and water depth in big bolder")
plt.savefig("./figures/s_e.png")
plt.show()
plt.close()

plt.plot(q1,sbi_b_num_f,'-',label="numerical data")
plt.errorbar(q1,sbi_b_exp_f,yerr=sbi_b_exp_f_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Force(N)')
plt.title("Total force acts on big bolder in inline arrangement")
plt.savefig("./figures/sbiBigForce.png")
plt.show()
plt.close()

plt.plot(q1,sbi_s_num_f,'-',label="numerical data")
plt.errorbar(q1,sbi_s_exp_f,yerr=sbi_s_exp_f_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Force(N)')
plt.title("Total force acts on small bolder in inline arrangement")
plt.savefig("./figures/sbiSmallForce.png")
plt.show()
plt.close()

plt.plot(q1,sbi_num_h,'-',label="numerical data")
plt.errorbar(q1,sbi_exp_h,yerr=sbi_exp_h_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Water depth(mm)')
plt.title("Water depth in inline arrangement")
plt.savefig("./figures/sbiDepth.png")
plt.show()
plt.close()


plt.plot(q1,sbs_b_num_f,'-',label="numerical data")
plt.errorbar(q1,sbs_b_exp_f,yerr=sbs_b_exp_f_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Force(N)')
plt.title("Total force acts on big bolder in staggered arrangement")
plt.savefig("./figures/sbsBigForce.png")
plt.show()
plt.close()

plt.plot(q1,sbs_s_num_f,'-',label="numerical data")
plt.errorbar(q1,sbs_s_exp_f,yerr=sbs_s_exp_f_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Force(N)')
plt.title("Total force acts on small bolder in staggered arrangement")
plt.savefig("./figures/sbsSmallForce.png")
plt.show()
plt.close()

plt.plot(q1,sbs_num_h,'-',label="numerical data")
plt.errorbar(q1,sbs_exp_h,yerr=sbs_exp_h_err,label="experimental data")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Water depth(mm)')
plt.title("Water depth in staggered arrangement")
plt.savefig("./figures/sbsDepth.png")
plt.show()
plt.close()

plt.plot(q1,i_b_e_f,label="Error in force (big bolder)")
plt.plot(q1,i_s_e_f,label="Error in force (small bolder)")
plt.plot(q1,i_e_h,label="Error in water depth")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Error (%)')
plt.title("Error in total force and water depth in inline arrangement")
plt.savefig("./figures/i_e.png")
plt.show()
plt.close()

plt.plot(q1,s_b_e_f,label="Error in force (big bolder)")
plt.plot(q1,s_s_e_f,label="Error in force (small bolder)")
plt.plot(q1,st_e_h,label="Error in water depth")
plt.legend()
plt.xlabel('Q(liter/sec)')
plt.ylabel('Error (%)')
plt.title("Error in total force and water depth in staggered arrangement")
plt.savefig("./figures/st_e.png")
plt.show()
plt.close()

